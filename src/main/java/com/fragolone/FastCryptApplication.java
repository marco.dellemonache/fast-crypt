package com.fragolone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastCryptApplication {
    public static void main(String[] args) {
        SpringApplication.run(FastCryptApplication.class, args);
    }
}