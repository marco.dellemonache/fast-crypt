package com.fragolone.controllers;

import com.fragolone.services.CryptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {

    @Autowired
    CryptoService cryptoService;

    @GetMapping("/encrypt/{password}")
    public String encrypt(@PathVariable String password) {
        return cryptoService.encrypt(password);
    }

    @GetMapping("/decrypt/{password}")
    public String decrypt(@PathVariable String password) {
        return cryptoService.decrypt(password);
    }
}
